from geopy.geocoders import Nominatim
from pycountry_convert import country_alpha2_to_continent_code
from mysql.connector import (connection)

geolocator = Nominatim()
continents = {"AF": "Africa", "AN": "Antarctica", "AS": "Asia", "EU": "Europe", "NA": "North America", "OC": "Oceania", "SA": "South America"}
selectResult = list()

cnx = connection.MySQLConnection(user='root', password='',
                                 host='127.0.0.1',
                                 database='twitter_credibility')

cursor = cnx.cursor()
query = ("SELECT tweet_id, longitude, latitude, credibility_score FROM tweet_collection")
cursor.execute(query)

for (tweet_id, longitude, latitude, credibility_score) in cursor:
    line = (tweet_id, longitude, latitude, credibility_score)
    selectResult.append(line)

cursor.close()

for tweet in selectResult:
    location = geolocator.reverse(str(tweet[2]) + "," + str(tweet[1]), language="en")
    loc = location.raw
    continent_name = ''
    try:
        cn_continent = country_alpha2_to_continent_code(loc['address']['country_code'].upper())
        continent_name = continents[cn_continent]

        cursor = cnx.cursor()
        add_location = ("INSERT INTO tweet_statistics2 (tweet_id, tweet_country, tweet_continent, tweet_credibility) VALUES (%s, %s, %s, %s)")
        data_location = (tweet[0], loc['address']['country'], continent_name, tweet[3])
        cursor.execute(add_location, data_location)
        cnx.commit()
        cursor.close()
    except:
        print('error')
    #print(str(tweet[2]) + "," + str(tweet[1]) + "# " + loc['address']['country'] + "," + continent_name)

cnx.close()






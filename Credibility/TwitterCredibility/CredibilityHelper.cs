﻿using NeuralNet.NeuralNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TwitterCredibilityAPI_Server.TwitterAPI;

namespace TwitterCredibilityAPI_Server
{
    public static class CredibilityHelper
    {
        public class UserCredibility
        {
            public double Score;

            public List<Tuple<long, double>> Tweets = new List<Tuple<long, double>>();
        }

        private static bool initialized = false;

        private static NeuralNetwork neuralNetwork;

        public static void Initialize()
        {
            // Network setup
            // - 0.01 learning rate
            // - 1 hidden layer (13 neurons), 6 neurons on input, 1 neuron on output { 6, 13, 1 }
            neuralNetwork = new NeuralNetwork(0.01, new int[] { 6, 13, 1 });
            
            initialized = true;
        }

        public static void Initialize(string modelFile)
        {
            neuralNetwork = NeuralNetwork.LoadNetwork(modelFile);

            initialized = true;
        }

        public static double GetCredibilityScore(TweetInfo tweetInfo)
        {
            if (!initialized)
            {
                throw new Exception("Not initialized");
            }

            var output = neuralNetwork.Run(tweetInfo.InputHashTags1());

            return output[0];
        }

        public static UserCredibility GetCredibilityScore(UserInfo userInfo)
        {
            if (!initialized)
            {
                throw new Exception("Not initialized");
            }

            var result = new UserCredibility();

            double lastTweetsAvg = 0.0;
            if (userInfo.LastTweets.Count() > 0)
            {
                foreach (var tweetInfo in userInfo.LastTweets)
                {
                    var credibility = GetCredibilityScore(tweetInfo);
                    lastTweetsAvg += credibility;

                    result.Tweets.Add(Tuple.Create(tweetInfo.ID, credibility));
                }
                lastTweetsAvg /= userInfo.LastTweets.Count();
            }

            double score = 0.0d;

            if (userInfo.HasLocation)
            {
                score += 0.01d;
            }

            if (userInfo.HasUrl)
            {
                score += 0.01d;
            }

            if (userInfo.HasDescription)
            {
                score += 0.03d;
            }

            if (userInfo.IsVerified)
            {
                score += 0.01d;
            }

            if (userInfo.GeoEnabled)
            {
                score += 0.08d;
            }

            score += userInfo.CreatedAt * 0.07d;
            score += lastTweetsAvg * 0.7d;

            result.Score = score;

            return result;
        }
    }
}

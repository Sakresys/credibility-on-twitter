﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace NeuralNet.NeuralNet
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Neuron
    {
        [JsonProperty]
        public readonly int Index;

        [JsonProperty]
        public List<Dendrite> Dendrites { get; set; }

        [JsonProperty]
        public double Bias { get; set; }

        [JsonProperty]
        public double Delta { get; set; }

        [JsonProperty]
        public double Value { get; set; }

        public int DendriteCount
        {
            get
            {
                return Dendrites.Count;
            }
        }

        public Neuron(int index)
        {
            Index = index;

            Random n = new Random(Environment.TickCount);
            Bias = n.NextDouble();

            Dendrites = new List<Dendrite>();
        }
    }


}

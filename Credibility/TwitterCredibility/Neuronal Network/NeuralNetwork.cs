﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNet.NeuralNet
{
    [JsonObject(MemberSerialization.OptIn)]
    public class NeuralNetwork
    {
        [JsonProperty]
        public List<Layer> Layers { get; set; }

        [JsonProperty]
        public double LearningRate { get; set; }

        public int LayerCount
        {
            get
            {
                return Layers.Count;
            }
        }

        public static NeuralNetwork LoadNetwork(string json)
        {
            var obj = JObject.Parse(json);

            double learningRate = obj.GetValue("LearningRate").Value<double>();

            var layers2 = obj.GetValue("Layers");

            List<int> layers = new List<int>();
            foreach (var layer in layers2)
            {
                var neurons = layer["Neurons"];
                layers.Add(neurons.Count());
            }

            var nn = new NeuralNetwork(learningRate, layers.ToArray());

            foreach (var layerToken in layers2)
            {
                var neurons = layerToken["Neurons"];
                layers.Add(neurons.Count());

                var layerIndex = layerToken["Index"].Value<int>();
                var layer = nn.Layers[layerIndex];
                foreach (var neuronToken in neurons)
                {
                    var neuronIndex = neuronToken["Index"].Value<int>();
                    var neuron = layer.Neurons[neuronIndex];

                    neuron.Bias = neuronToken["Bias"].Value<double>();
                    neuron.Delta = neuronToken["Delta"].Value<double>();
                    neuron.Value = neuronToken["Value"].Value<double>();

                    var dendriteTokens = neuronToken["Dendrites"];
                    foreach (var dendriteToken in dendriteTokens)
                    {
                        var dendrite = neuron.Dendrites[dendriteToken["Index"].Value<int>()];

                        dendrite.Weight = dendriteToken["Weight"].Value<double>();
                    }
                }
            }

            return nn;

            //var nn = new NeuralNetwork();

            //return nn;
        }

        public void SaveModel(Stream destination)
        {
            var x = JsonConvert.SerializeObject(this);
            
            var writer = new StreamWriter(destination);
            writer.Write(x);

            writer.Flush();
        }

        public NeuralNetwork(double learningRate, int[] layers)
        {
            if (layers.Length < 2) return;

            this.LearningRate = learningRate;
            this.Layers = new List<Layer>();

            for(int layerIndex = 0; layerIndex < layers.Length; layerIndex++)
            {
                Layer layer = new Layer(layerIndex, layers[layerIndex]);
                this.Layers.Add(layer);

                for (int neuronIndex = 0; neuronIndex < layers[layerIndex]; neuronIndex++)
                {
                    layer.Neurons.Add(new Neuron(neuronIndex));
                }

                layer.Neurons.ForEach((nn) =>
                {
                    if (layerIndex == 0)
                    {
                        nn.Bias = 0;
                    }
                    else
                    {
                        for (int d = 0; d < layers[layerIndex - 1]; d++)
                        {
                            nn.Dendrites.Add(new Dendrite(d));
                        }
                    }
                });
            }
        }

        private double Sigmoid(double x)
        {
            return 1 / (1 + Math.Exp(-x));
        }

        public double[] Run(List<double> input)
        {
            if (input.Count != this.Layers[0].NeuronCount)
            {
                return null;
            }

            for (int l = 0; l < Layers.Count; l++)
            {
                Layer layer = Layers[l];

                for (int n = 0; n < layer.Neurons.Count; n++)
                {
                    Neuron neuron = layer.Neurons[n];

                    if (l == 0)
                    {
                        neuron.Value = input[n];
                    }
                    else
                    {
                        neuron.Value = 0;
                        for (int np = 0; np < this.Layers[l - 1].Neurons.Count; np++)
                        {
                            neuron.Value = neuron.Value + this.Layers[l - 1].Neurons[np].Value * neuron.Dendrites[np].Weight;
                        }

                        neuron.Value = Sigmoid(neuron.Value + neuron.Bias);
                    }
                }
            }

            Layer last = Layers[Layers.Count - 1];
            int numOutput = last.Neurons.Count ;
            double[] output = new double[numOutput];
            for (int i = 0; i < last.Neurons.Count; i++)
            {
                output[i] = last.Neurons[i].Value;
            }

            return output;
        }

        public bool Train(List<double> input, List<double> output)
        {
            if ((input.Count != this.Layers[0].Neurons.Count) || (output.Count != this.Layers[this.Layers.Count - 1].Neurons.Count)) return false;

            Run(input);

            for(int i = 0; i < this.Layers[this.Layers.Count - 1].Neurons.Count; i++)
            {
                Neuron neuron = this.Layers[this.Layers.Count - 1].Neurons[i];

                neuron.Delta = neuron.Value * (1 - neuron.Value) * (output[i] - neuron.Value);

                for(int j = this.Layers.Count - 2; j >= 1; j--)
                {
                    for(int k = 0; k < this.Layers[j].Neurons.Count; k++)
                    {
                        Neuron n = this.Layers[j].Neurons[k];

                        n.Delta = n.Value *
                                  (1 - n.Value) *
                                  this.Layers[j + 1].Neurons[i].Dendrites[k].Weight *
                                  this.Layers[j + 1].Neurons[i].Delta;
                    }
                }
            }

            for(int i = this.Layers.Count - 1; i >= 1; i--)
            {
                for(int j=0; j < this.Layers[i].Neurons.Count; j++)
                {
                    Neuron n = this.Layers[i].Neurons[j];
                    n.Bias = n.Bias + (this.LearningRate * n.Delta);

                    for (int k = 0; k < n.Dendrites.Count; k++)
                        n.Dendrites[k].Weight = n.Dendrites[k].Weight + (this.LearningRate * this.Layers[i - 1].Neurons[k].Value * n.Delta);
                }
            }

            return true;
        }

    }
}

﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace NeuralNet.NeuralNet
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Layer
    {
        [JsonProperty]
        public readonly int Index;

        [JsonProperty]
        public List<Neuron> Neurons { get; set; }
        
        public int NeuronCount
        {
            get
            {
                return Neurons.Count;
            }
        }

        public Layer(int index, int numNeurons)
        {
            Index = index;
            Neurons = new List<Neuron>(numNeurons);
        }
    }
}

﻿using Newtonsoft.Json;

namespace NeuralNet.NeuralNet
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Dendrite
    {
        [JsonProperty]
        public readonly int Index;

        [JsonProperty]
        public double Weight { get; set; }

        public Dendrite(int index)
        {
            Index = index;

            CryptoRandom n = new CryptoRandom();
            this.Weight = n.RandomValue;
        }
    }

}

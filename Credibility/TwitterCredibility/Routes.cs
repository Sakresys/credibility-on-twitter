﻿using Grapevine.Interfaces.Server;
using Grapevine.Server;
using Grapevine.Server.Attributes;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TwitterCredibilityAPI_Server;
using TwitterCredibilityAPI_Server.Monitor;
using static TwitterCredibilityAPI_Server.Monitor.Monitor;

namespace TwitterCredibility
{
    [RestResource]
    public class Routes
    {
        [RestRoute(HttpMethod = Grapevine.Shared.HttpMethod.GET, PathInfo = @"^/credibility/users/[a-zA-Z0-9_]+$")]
        public IHttpContext GetUserCredibility(IHttpContext context)
        {
            var param = context.Request.PathInfo.Substring(context.Request.PathInfo.LastIndexOf('/') + 1);

            var userCredibility = CredibilityHelper.GetCredibilityScore(TwitterAPI.GetUserInfo(param));
            JObject response = new JObject(new JProperty("credibility", userCredibility.Score));

            context.Response.Headers["Access-Control-Allow-Origin"] = "*";
            context.Response.SendResponse(response.ToString());
            return context;
        }

        [RestRoute(HttpMethod = Grapevine.Shared.HttpMethod.GET, PathInfo = @"^/credibility/tweets/\d+$")]
        public IHttpContext GetTweetCredibility(IHttpContext context)
        {
            var param = context.Request.PathInfo.Substring(context.Request.PathInfo.LastIndexOf('/') + 1);

            var tweetCredibility = CredibilityHelper.GetCredibilityScore(TwitterAPI.GetTweet(long.Parse(param)));
            JObject response = new JObject(new JProperty("credibility", tweetCredibility));

            context.Response.Headers["Access-Control-Allow-Origin"] = "*";
            context.Response.SendResponse(response.ToString());
            return context;
        }

        [RestRoute(HttpMethod = Grapevine.Shared.HttpMethod.POST, PathInfo = @"^/credibility/users/monitor/[a-zA-Z0-9_]+$")]
        [RestRoute(HttpMethod = Grapevine.Shared.HttpMethod.OPTIONS, PathInfo = @"^/credibility/users/monitor/[a-zA-Z0-9_]+$")]
        public IHttpContext MonitorUser(IHttpContext context)
        {
            JObject response = new JObject();
            if (context.Request.HttpMethod == Grapevine.Shared.HttpMethod.POST)
            {
                var param = context.Request.PathInfo.Substring(context.Request.PathInfo.LastIndexOf('/') + 1);

                var id = Instance.MonitorUser(param);
                response = new JObject(new JProperty("monitoringRequestId", id));
            }

            context.Response.Headers["Access-Control-Allow-Methods"] = "POST, OPTIONS";
            context.Response.Headers["Access-Control-Allow-Origin"] = "*";
            context.Response.SendResponse(response.ToString());
            return context;
        }

        [RestRoute(HttpMethod = Grapevine.Shared.HttpMethod.POST, PathInfo = @"^/credibility/tweets/monitor/\d+$")]
        [RestRoute(HttpMethod = Grapevine.Shared.HttpMethod.OPTIONS, PathInfo = @"^/credibility/tweets/monitor/\d+$")]
        public IHttpContext MonitorTweet(IHttpContext context)
        {
            JObject response = new JObject();
            if (context.Request.HttpMethod == Grapevine.Shared.HttpMethod.POST)
            {
                var param = context.Request.PathInfo.Substring(context.Request.PathInfo.LastIndexOf('/') + 1);

                var id = Instance.MonitorTweet(long.Parse(param));
                response = new JObject(new JProperty("monitoringRequestId", id));
            }

            context.Response.Headers["Access-Control-Allow-Methods"] = "POST, OPTIONS";
            context.Response.Headers["Access-Control-Allow-Origin"] = "*";
            context.Response.SendResponse(response.ToString());
            return context;
        }

        [RestRoute(HttpMethod = Grapevine.Shared.HttpMethod.GET, PathInfo = @"^/credibility/monitor/\d+$")]
        public IHttpContext GetMonitoredId(IHttpContext context)
        {
            var param = context.Request.PathInfo.Substring(context.Request.PathInfo.LastIndexOf('/') + 1);

            MonitoringResults results = Instance.GetMonitoringResults(int.Parse(param));
            JObject response = new JObject(new JProperty("Type", results.Type.ToString()),
                new JProperty("Score", results.Score),
                new JProperty("Subject", results.Subject),
                new JProperty("StartDateTime", results.StartDateTime));

            JArray history = new JArray();

            if (results.Type == MonitoringRequest.RequestTypes.Tweet)
            {
                for (int i = 0; i < ((TweetMonitoringResults)results).History.Count; ++i)
                {
                    JObject tweet = new JObject(new JProperty("Id", ((TweetMonitoringResults)results).History[i].Id),
                        new JProperty("Score", ((TweetMonitoringResults)results).History[i].Score),
                        new JProperty("CheckDateTime", ((TweetMonitoringResults)results).History[i].CheckDateTime));
                    history.Add(tweet);
                }
            }
            else
            {
                for (int i = 0; i < ((UserMonitoringResults)results).History.Count; ++i)
                {
                    JObject user = new JObject(new JProperty("Score", ((UserMonitoringResults)results).History[i].Score));
                    JArray userTweets = new JArray();
                    for(int j = 0; j < ((UserMonitoringResults)results).History[i].Tweets.Count; ++j)
                    {
                        JObject tweet = new JObject(new JProperty("Id", ((UserMonitoringResults)results).History[i].Tweets[j].Id),
                        new JProperty("Score", ((UserMonitoringResults)results).History[i].Tweets[j].Score),
                        new JProperty("CheckDateTime", ((UserMonitoringResults)results).History[i].Tweets[j].CheckDateTime));
                        userTweets.Add(tweet);
                    }
                    user.Add("Tweets", userTweets);
                    history.Add(user);
                }
            }

            response.Add("History", history);

            context.Response.Headers["Access-Control-Allow-Origin"] = "*";
            context.Response.SendResponse(response.ToString());
            return context;
        }

        [RestRoute]
        public IHttpContext HandleAllGetRequests(IHttpContext context)
        {
            return context;
        }
    }
}

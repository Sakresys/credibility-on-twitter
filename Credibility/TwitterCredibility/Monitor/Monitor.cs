﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TwitterCredibilityAPI_Server.Monitor
{
    class Monitor
    {
        private static readonly string ConnectionString = "server=localhost;userid=root;database=twitter_credibility";

        private readonly MySqlConnection monitorConnection;
        private readonly MySqlConnection logConnection;
        private readonly MySqlConnection resultsConnection;

        private bool running = false;

        private static Monitor instance = null;
        public static Monitor Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Monitor();
                }

                return instance;
            }
        }

        private Monitor()
        {
            monitorConnection = new MySqlConnection(ConnectionString);
            monitorConnection.Open();

            logConnection = new MySqlConnection(ConnectionString);
            logConnection.Open();

            resultsConnection = new MySqlConnection(ConnectionString);
            resultsConnection.Open();
        }

        /// <summary>
        /// Insert monitoring request into database
        /// </summary>
        /// <param name="tweetId">Id of the tweet to be monitored</param>
        /// <returns>Monitoring request id</returns>
        public int MonitorTweet(long tweetId)
        {
            int monitoringRequestId = -1;

            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();

                using (var q = new MySqlCommand("INSERT INTO monitoring_requests (type, subject) VALUES (?type, ?subject)", conn))
                {
                    q.Parameters.Add("?type", MySqlDbType.Int32).Value = 1;
                    q.Parameters.Add("?subject", MySqlDbType.String).Value = tweetId.ToString();

                    q.ExecuteNonQuery();

                    monitoringRequestId = (int)q.LastInsertedId;
                }

                conn.Close();
            }

            return monitoringRequestId;
        }

        public int MonitorUser(string screenName)
        {
            int monitoringRequestId = -1;

            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();

                using (var q = new MySqlCommand("INSERT INTO monitoring_requests (type, subject) VALUES (?type, ?subject)", conn))
                {
                    q.Parameters.Add("?type", MySqlDbType.Int32).Value = 2;
                    q.Parameters.Add("?subject", MySqlDbType.String).Value = screenName;

                    q.ExecuteNonQuery();

                    monitoringRequestId = (int)q.LastInsertedId;
                }

                conn.Close();
            }

            return monitoringRequestId;
        }

        public void Start()
        {
            running = true;

            Task.Delay(GetSpanUntilNextHour()).ContinueWith(task => MonitorWorker());
        }

        private TimeSpan GetSpanUntilNextHour()
        {
            //return TimeSpan.FromSeconds(20);

            var now = DateTime.Now.AddHours(1);
            var nextHour = new DateTime(now.Year, now.Month, now.Day, now.Hour, 0, 0);

            return nextHour - DateTime.Now;
        }

        public class MonitoringRequest
        {
            public enum RequestTypes
            {
                Tweet, User
            }

            public int Id;
            public RequestTypes Type;
            public string Subject;

            public long GetSubjectAsLong()
            {
                return long.Parse(Subject);
            }

            public string GetSubjectAsString()
            {
                return Subject;
            }
        }

        private List<MonitoringRequest> GetActiveMonitoringRequests()
        {
            List<MonitoringRequest> activeMonitoringRequests = new List<MonitoringRequest>();

            var now = DateTime.Now;

            DateTime startDate = new DateTime(now.Year, now.Month, now.Day, now.Hour, 0, 0).AddDays(-1);

            using (var q = new MySqlCommand("SELECT id, type, subject, start_date FROM monitoring_requests WHERE start_date >= ?startDate", monitorConnection))
            {
                q.Parameters.Add("?startDate", MySqlDbType.DateTime).Value = startDate;

                using (var reader = q.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var monitoringRequest = new MonitoringRequest()
                        {
                            Id = reader.GetInt32("id"),
                            Subject = reader.GetString("subject"),
                            Type = reader.GetInt32("type") == 1 ? MonitoringRequest.RequestTypes.Tweet : MonitoringRequest.RequestTypes.User
                        };

                        activeMonitoringRequests.Add(monitoringRequest);
                    }
                }
            }

            return activeMonitoringRequests;
        }

        private void LogTweetResult(long tweetId, float credibilityScore)
        {
            using (var q = new MySqlCommand("INSERT INTO monitored_tweets (tweetId, credibility_score) VALUES (?tweetId, ?score)", logConnection))
            {
                q.Parameters.Add("?tweetId", MySqlDbType.Int64).Value = tweetId;
                q.Parameters.Add("?score", MySqlDbType.Float).Value = credibilityScore;
                q.ExecuteNonQuery();
            }
        }

        private void LogUserResult(string screenName, CredibilityHelper.UserCredibility userCredibility)
        {
            using (var q = new MySqlCommand("INSERT INTO monitored_users (screenName, tweets, credibility_score) VALUES (?screenName, ?tweets, ?score)", logConnection))
            {
                q.Parameters.Add("?screenName", MySqlDbType.String).Value = screenName;
                q.Parameters.Add("?tweets", MySqlDbType.String).Value = string.Join(",", userCredibility.Tweets.Select(pair => pair.Item1));
                q.Parameters.Add("?score", MySqlDbType.Float).Value = (float)userCredibility.Score;
                q.ExecuteNonQuery();
            }

            foreach (var tweet in userCredibility.Tweets)
            {
                LogTweetResult(tweet.Item1, (float)tweet.Item2);
            }
        }

        private void MonitorWorker()
        {
            if (!running)
            {
                return;
            }

            Console.WriteLine("[{0}] MonitorWorker", DateTime.Now);
            
            var activeMonitoringRequests = GetActiveMonitoringRequests();
            foreach (var monitoringRequest in activeMonitoringRequests)
            {
                try
                {
                    if (monitoringRequest.Type == MonitoringRequest.RequestTypes.Tweet)
                    {
                        var tweetCredibility = CredibilityHelper.GetCredibilityScore(TwitterAPI.GetTweet(monitoringRequest.GetSubjectAsLong()));
                        LogTweetResult(monitoringRequest.GetSubjectAsLong(), (float)tweetCredibility);
                    }
                    else
                    {
                        var userCredibility = CredibilityHelper.GetCredibilityScore(TwitterAPI.GetUserInfo(monitoringRequest.GetSubjectAsString()));
                        LogUserResult(monitoringRequest.GetSubjectAsString(), userCredibility);
                    }
                }
                catch
                {
                    Console.WriteLine("Failed to process a monitoring request {0}, {1}", monitoringRequest.Id, monitoringRequest.Subject);
                }

                // Delay requests by 10 seconds so we will not exceed the limit of 
                // requests per minute so fast.
                Thread.Sleep(TimeSpan.FromSeconds(10));
            }

            Task.Delay(GetSpanUntilNextHour()).ContinueWith(task => MonitorWorker());
        }

        public MonitoringResults GetMonitoringResults(int monitoringRequestId)
        {
            MonitoringResults monitoringResults = null;

            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();

                int type = -1;
                string subject = "";
                DateTime start_date = DateTime.MinValue;

                using (var q = new MySqlCommand("SELECT type, subject, start_date FROM monitoring_requests WHERE id = ?id", conn))
                {
                    q.Parameters.Add("?id", MySqlDbType.Int32).Value = monitoringRequestId;

                    using (var reader = q.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            type = reader.GetInt32("type");
                            subject = reader.GetString("subject");
                            start_date = reader.GetDateTime("start_date");
                        }
                    }
                }

                if (type > -1)
                {
                    if (type == 1)
                    {
                        #region Tweet
                        var results = new TweetMonitoringResults()
                        {
                            Id = monitoringRequestId,
                            Type = MonitoringRequest.RequestTypes.Tweet,
                            Subject = subject,
                            Score = -1,
                            StartDateTime = start_date
                        };
                        // type = tweet => get tweet id records from start till start + 24h

                        results.History = GetTweetHistory(long.Parse(subject), start_date, start_date.AddDays(1));
                        if (results.History.Count > 0)
                        {
                            results.Score = results.History.First().Score;
                        }

                        monitoringResults = results;
                        #endregion
                    }
                    else
                    {
                        #region User
                        var results = new UserMonitoringResults()
                        {
                            Id = monitoringRequestId,
                            Type = MonitoringRequest.RequestTypes.User,
                            Subject = subject,
                            Score = -1,
                            StartDateTime = start_date
                        };

                        // type = user => get user id .... + tweets
                        results.History = GetUserHistory(subject, start_date, start_date.AddDays(1));
                        if (results.History.Count > 0)
                        {
                            results.Score = results.History.First().Score;
                        }

                        monitoringResults = results;
                        #endregion
                    }
                }

                conn.Close();
            }

            return monitoringResults;
        }

        private List<TweetMonitorResult> GetTweetHistory(long tweetId, DateTime startDate, DateTime finishDate)
        {
            Dictionary<DateTime, TweetMonitorResult> results = new Dictionary<DateTime, TweetMonitorResult>();

            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();

                using (var q = new MySqlCommand("SELECT check_date_time, credibility_score FROM monitored_tweets WHERE tweetId = ?id AND (check_date_time BETWEEN ?start_date AND ?finish_data) ORDER BY check_date_time DESC", conn))
                {
                    q.Parameters.Add("?id", MySqlDbType.Int64).Value = tweetId;
                    q.Parameters.Add("?start_date", MySqlDbType.DateTime).Value = startDate;
                    q.Parameters.Add("?finish_data", MySqlDbType.DateTime).Value = finishDate;

                    using (var reader = q.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DateTime checkTime = reader.GetDateTime("check_date_time");
                            float score = reader.GetFloat("credibility_score");
                            var realDate = new DateTime(checkTime.Year, checkTime.Month, checkTime.Day, checkTime.Hour, 0, 0);

                            results[realDate] = new TweetMonitorResult(tweetId, score, realDate);
                        }
                    }
                }

                

                conn.Close();
            }

            return new List<TweetMonitorResult>(results.Select(pair => pair.Value));
        }

        private TweetMonitorResult GetTweetResultAt(long tweetId, DateTime dateTime)
        {
            TweetMonitorResult result = null;

            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();

                var start = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, 0, 0);

                using (var q = new MySqlCommand("SELECT check_date_time, credibility_score FROM monitored_tweets WHERE tweetId = ?id AND (check_date_time BETWEEN ?start_date AND ?finish_data) LIMIT 1", conn))
                {
                    q.Parameters.Add("?id", MySqlDbType.Int64).Value = tweetId;
                    q.Parameters.Add("?start_date", MySqlDbType.DateTime).Value = start;
                    q.Parameters.Add("?finish_data", MySqlDbType.DateTime).Value = start.AddMinutes(59);

                    using (var reader = q.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DateTime checkTime = reader.GetDateTime("check_date_time");
                            float score = reader.GetFloat("credibility_score");

                            result = new TweetMonitorResult(tweetId, score, checkTime);
                            break;
                        }
                    }
                }

                conn.Close();
            }

            return result;
        }

        private List<UserHistoryMonitoringResults> GetUserHistory(string screenName, DateTime startDate, DateTime finishDate)
        {
            Dictionary<DateTime, UserHistoryMonitoringResults> history = new Dictionary<DateTime, UserHistoryMonitoringResults>();

            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();

                using (var q = new MySqlCommand("SELECT check_date_time, tweets, credibility_score FROM monitored_users WHERE screenName = ?screenName AND (check_date_time BETWEEN ?start_date AND ?finish_date) ORDER BY check_date_time DESC", conn))
                {
                    q.Parameters.Add("?screenName", MySqlDbType.String).Value = screenName;
                    q.Parameters.Add("?start_date", MySqlDbType.DateTime).Value = startDate;
                    q.Parameters.Add("?finish_date", MySqlDbType.DateTime).Value = finishDate;

                    using (var reader = q.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DateTime checkTime = reader.GetDateTime("check_date_time");
                            float score = reader.GetFloat("credibility_score");
                            string[] tweets = reader.GetString("tweets").Split(new char[] { ',' });
                            var realDate = new DateTime(checkTime.Year, checkTime.Month, checkTime.Day, checkTime.Hour, 0, 0);

                            var userResults = new UserHistoryMonitoringResults();
                            userResults.Score = score;

                            foreach (var tweetId in tweets)
                            {
                                userResults.Tweets.Add(GetTweetResultAt(long.Parse(tweetId), realDate));
                            }
                            
                            history[realDate] = userResults;
                        }
                    }
                }

                conn.Close();
            }

            return new List<UserHistoryMonitoringResults>(history.Select(pair => pair.Value));
        }

        public class MonitoringResults
        {
            public int Id;

            [JsonConverter(typeof(StringEnumConverter))]
            public MonitoringRequest.RequestTypes Type;

            public string Subject;
            public float Score;
            public DateTime StartDateTime;
        }

        public class TweetMonitoringResults : MonitoringResults
        {
            public List<TweetMonitorResult> History = new List<TweetMonitorResult>();
        }

        public class TweetMonitorResult
        {
            public readonly string Id;
            public readonly float Score;
            public readonly DateTime CheckDateTime;

            public TweetMonitorResult(long id, float score, DateTime checkDateTime)
            {
                Id = id.ToString();
                Score = score;
                CheckDateTime = checkDateTime;
            }
        }

        public class UserHistoryMonitoringResults
        {
            public float Score;
            public List<TweetMonitorResult> Tweets = new List<TweetMonitorResult>();
        }

        public class UserMonitoringResults : MonitoringResults
        {
            public List<UserHistoryMonitoringResults> History = new List<UserHistoryMonitoringResults>();
        }
    }
}

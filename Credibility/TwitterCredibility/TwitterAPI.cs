﻿using edu.stanford.nlp.ling;
using edu.stanford.nlp.neural.rnn;
using edu.stanford.nlp.pipeline;
using edu.stanford.nlp.sentiment;
using edu.stanford.nlp.trees;
using edu.stanford.nlp.util;
using java.util;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Tweetinvi;
using static edu.stanford.nlp.ling.CoreAnnotations;

namespace TwitterCredibilityAPI_Server
{
    public static class TwitterAPI
    {
        private const string CONSUMER_KEY = "";
        private const string CONSUMER_SECRET = "";
        private const string ACCESS_TOKEN = "";
        private const string ACCESS_TOKEN_SECRET = "";      

        private static readonly string[] stopwords = { "a", "about", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "as", "at", "be", "because", "been", "before", "being", "below", "between", "both", "but", "by", "could", "did", "do", "does", "doing", "down", "during", "each", "few", "for", "from", "further", "had", "has", "have", "having", "he", "he'd", "he'll", "he's", "her", "here", "here's", "hers", "herself", "him", "himself", "his", "how", "how's", "i", "i'd", "i'll", "i'm", "i've", "if", "in", "into", "is", "it", "it's", "its", "itself", "let's", "me", "more", "most", "my", "myself", "nor", "of", "on", "once", "only", "or", "other", "ought", "our", "ours", "ourselves", "out", "over", "own", "same", "she", "she'd", "she'll", "she's", "should", "so", "some", "such", "than", "that", "that's", "the", "their", "theirs", "them", "themselves", "then", "there", "there's", "these", "they", "they'd", "they'll", "they're", "they've", "this", "those", "through", "to", "too", "under", "until", "up", "very", "was", "we", "we'd", "we'll", "we're", "we've", "were", "what", "what's", "when", "when's", "where", "where's", "which", "while", "who", "who's", "whom", "why", "why's", "with", "would", "you", "you'd", "you'll", "you're", "you've", "your", "yours", "yourself", "yourselves" };
        private static readonly Regex punctuation = new Regex("\\p{P}+");

        private static StanfordCoreNLP pipeline;

        public static void Initialize()
        {
            var auth = Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

            SetupStanfordNLPCore();
        }

        private static void SetupStanfordNLPCore()
        {
            var jarRoot = @"..\..\..\stanford-english-corenlp-3.8.0-models\";

            var props = new Properties();
            props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");

            var curDir = Environment.CurrentDirectory;
            Directory.SetCurrentDirectory(jarRoot);

            pipeline = new StanfordCoreNLP(props);
            Directory.SetCurrentDirectory(curDir);
        }

        public class TweetInfo
        {
            public long ID;

            //For training data///////
            public long AuthorID;
            public int RetweetsNumber;
            public int FavoritesNumber;
            public DateTime CreatedAt;
            public int WordsCount;
            public int RelevantWordsCount;
            public int CharactersCount;
            ///////////////////////////

            public string Text;

            public double RetweetsScore;
            public double FavouritesScore;
            public double RelevantWordsRatio;
            public double SentimentScore;

            public float CredibilityScore;

            private readonly List<double> input = new List<double>();

            public List<double> Input()
            {
                if (input.Count == 0)
                {
                    input.Add(RetweetsScore);
                    input.Add(FavouritesScore);
                    input.Add(RelevantWordsRatio);
                    input.Add(SentimentScore);
                }

                return input;
            }

            public List<double> InputNoSentiments()
            {
                if (input.Count == 0)
                {
                    input.Add(RetweetsScore);
                    input.Add(FavouritesScore);
                    input.Add(RelevantWordsRatio);
                }

                return input;
            }

            public List<double> InputHashTags12()
            {
                if (input.Count == 0)
                {
                    input.Add(RetweetsScore);
                    input.Add(FavouritesScore);
                    input.Add(RelevantWordsRatio);
                    input.Add(SentimentScore);
                    // hashtags length / tweet length

                    int hashtagsCount = 0;
                    int hashtagsCharacters = 0;
                    var words = Text.Split(new char[] { ' ' });
                    foreach (var word in words)
                    {
                        if (string.IsNullOrEmpty(word))
                        {
                            continue;
                        }

                        if (word[0] == '#')
                        {
                            ++hashtagsCount;
                            hashtagsCharacters += word.Length - 1;
                        }
                    }

                    input.Add(hashtagsCount);
                    input.Add(hashtagsCharacters == 0 ? 0 : Text.Length / hashtagsCharacters);
                }

                return input;
            }

            public List<double> InputHashTags1()
            {
                if (input.Count == 0)
                {
                    input.Add(RetweetsScore);
                    input.Add(FavouritesScore);
                    input.Add(RelevantWordsRatio);
                    input.Add(SentimentScore);

                    int hashtagsCount = 0;
                    var words = Text.Split(new char[] { ' ' });
                    foreach (var word in words)
                    {
                        if (string.IsNullOrEmpty(word))
                        {
                            continue;
                        }

                        if (word[0] == '#')
                        {
                            ++hashtagsCount;
                        }
                    }


                    input.Add(hashtagsCount);
                }

                return input;
            }

            public List<double> InputHashTags2()
            {
                if (input.Count == 0)
                {
                    input.Add(RetweetsScore);
                    input.Add(FavouritesScore);
                    input.Add(RelevantWordsRatio);
                    input.Add(SentimentScore);
                    // hashtags length / tweet length

                    int hashtagsCharacters = 0;
                    var words = Text.Split(new char[] { ' ' });
                    foreach (var word in words)
                    {
                        if (string.IsNullOrEmpty(word))
                        {
                            continue;
                        }

                        if (word[0] == '#')
                        {
                            hashtagsCharacters += word.Length - 1;
                        }
                    }

                    input.Add(hashtagsCharacters == 0 ? 0 : Text.Length / hashtagsCharacters);
                }

                return input;
            }

            List<double> output = new List<double>();
            public List<double> Output()
            {
                if (output.Count == 0)
                {
                    output.Add(CredibilityScore);
                    //output.Add(1 - CredibilityScore);
                }

                return output;
            }
        }

        public class UserInfo
        {
            public string ScreenName;
            public bool HasLocation;
            public bool HasUrl;
            public bool HasDescription;
            public bool IsVerified;
            public bool GeoEnabled;

            ///////For training data
            public int FollowersNumber;
            public long ID;
            //////////////////////////

            public double CreatedAt;

            public List<TweetInfo> LastTweets = new List<TweetInfo>();
        }

        public static TweetInfo GetTweet(long id)
        {
            var tweet = Tweet.GetTweet(id);
            if (tweet == null)
            {
                return null;
            }

            return ConvertTweet(tweet);
        }

        public static double GetTweetSentiment(String text)
        {
            var annotation = new Annotation(text);
            pipeline.annotate(annotation);

            return GetTweetSentiment(annotation);
        }

        public static double GetTweetSentiment(Annotation annotation)
        {
            float sentiment = 0.0f;
            var sentences = annotation.get(typeof(CoreAnnotations.SentencesAnnotation));
            foreach (CoreMap sentence in sentences as ArrayList)
            {
                Tree tree = (Tree)sentence.get(typeof(SentimentCoreAnnotations.SentimentAnnotatedTree));
                int sentimentValue = RNNCoreAnnotations.getPredictedClass(tree);
                switch (sentimentValue)
                {
                    case 0://very negative
                        sentiment += 0.75f;
                        break;
                    case 1://negative
                        sentiment += 0.5f;
                        break;
                    case 2://neutral
                        sentiment += 0.0f;
                        break;
                    case 3://positive
                        sentiment += 0.25f;
                        break;
                    case 4://very positive
                        sentiment += 0.5f;
                        break;
                    default:
                        sentiment += 0.0f;
                        break;
                }
            }
            sentiment /= (sentences as ArrayList).size();

            return sentiment;
        }

        public static TweetInfo ConvertTweet(Tweetinvi.Models.ITweet tweet)
        {
            var annotation = new Annotation(tweet.Text);
            pipeline.annotate(annotation);

            ArrayList tokens = (ArrayList)annotation.get(typeof(TokensAnnotation));
            List<string> words = new List<string>();
            foreach (CoreLabel word in tokens)
            {
                words.Add(word.word());
            }
            words = words.Where(x => !stopwords.Contains(x) && !punctuation.IsMatch(x)).ToList();

            int retweetsCount = tweet.RetweetCount;
            int favouritesCount = tweet.FavoriteCount;

            int authorFollowersCount = tweet.CreatedBy.FollowersCount;

            float reachableFollowers = ((float)authorFollowersCount * 0.03f) / 100.0f;

            float retweetsScore = 0.0f;
            if (retweetsCount > 0)
            {
                if (retweetsCount > reachableFollowers)
                {
                    retweetsScore = 1.0f;
                }
                else
                {
                    retweetsScore = Math.Min(1, (float)retweetsCount / reachableFollowers);
                }
            }

            float favouritesScore = 0.0f;
            if (favouritesCount > 0)
            {
                if (favouritesCount > reachableFollowers)
                {
                    favouritesScore = 1.0f;
                }
                else
                {
                    //favouritesScore = 1.0f / (reachableFollowers - favouritesCount);
                    favouritesScore = Math.Min(1, (float)favouritesCount / reachableFollowers);
                }
            }

            int relevantWordsCount = words.Count();
            int wordsCount = tokens.size();

            float relevantWordsRatio = (float)relevantWordsCount / wordsCount;

            var tweetInfo = new TweetInfo()
            {
                ID = tweet.Id,
                FavouritesScore = favouritesScore,
                RetweetsScore = retweetsScore,
                RelevantWordsRatio = relevantWordsRatio,
                Text = tweet.Text,

                //Training data
                AuthorID = tweet.CreatedBy.Id,
                RetweetsNumber = tweet.RetweetCount,
                FavoritesNumber = tweet.FavoriteCount,
                CreatedAt = tweet.CreatedAt,
                WordsCount = tokens.size(),
                RelevantWordsCount = relevantWordsCount,
                CharactersCount = tweet.Text.Length,
                SentimentScore = GetTweetSentiment(annotation)
            };

            return tweetInfo;
        }

        public static UserInfo GetUserInfo(string screenName, int tweetsCount = 20)
        {
            var lastTweets = Timeline.GetUserTimeline(screenName, tweetsCount);

            if (lastTweets.Count() == 0)
            {
                return null;
            }

            var user = lastTweets.ElementAt(0).CreatedBy;

            var userInfo = new UserInfo()
            {
                ScreenName = screenName,
                HasLocation = user.Location != null && user.Location.Length > 10,
                HasDescription = user.Description != null && user.Description.Length > 10,
                HasUrl = user.Url != null && user.Url.Length > 0,
                IsVerified = user.Verified,
                GeoEnabled = user.GeoEnabled,
                CreatedAt = ((DateTime.Now - user.CreatedAt).TotalDays / 31) / ((DateTime.Now - DateTime.Parse("July 15, 2006")).TotalDays / 30),
                LastTweets = new List<TweetInfo>(lastTweets.Select(t => ConvertTweet(t))),

                //Training data
                FollowersNumber = user.FollowersCount,
                ID = user.Id
            };

            return userInfo;
        }
    }
}

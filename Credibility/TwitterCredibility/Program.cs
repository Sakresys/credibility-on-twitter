﻿using edu.stanford.nlp.ling;
using edu.stanford.nlp.neural.rnn;
using edu.stanford.nlp.pipeline;
using edu.stanford.nlp.sentiment;
using edu.stanford.nlp.trees;
using edu.stanford.nlp.util;
using java.util;
using System;
using System.IO;

namespace TwitterCredibilityAPI_Server
{
    class Program
    {
        private static string modelsLocation = @"..\..\..\Modele Antrenate\";

        static void Main(string[] args)
        {
            TwitterAPI.Initialize();
            //if you modify model, also modify in tweet collection and modify in credibility helper in GetCredibilityScore the method used
            CredibilityHelper.Initialize(File.ReadAllText(modelsLocation + "model_100k_vers5_1000.txt"));
            Monitor.Monitor.Instance.Start();

            using (var server = new Grapevine.Server.RestServer())
            {
                server.LogToConsole();

                server.Host = "127.0.0.1";
                server.Port = "1231";
                server.Start();

                Console.Read();

                server.Stop();
            }
        }
    }
}

﻿using MySql.Data.MySqlClient;
using NeuralNet.NeuralNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TwitterCredibilityAPI_Server;
using static TwitterCredibilityAPI_Server.TwitterAPI;

namespace UsersCredibility
{
    public class Program
    {
        private const string CONNECTION_STRING = "server=localhost;userid=root;database=twitter_credibility";
        private static string modelsLocation = @"..\..\..\Modele Antrenate\";
        private static string resultPath;

        public static List<TweetInfo> GetDataset()
        {
            List<TweetInfo> dataset = new List<TweetInfo>();

            using (var conn = new MySqlConnection(CONNECTION_STRING))
            {
                conn.Open();

                using (var cmd = new MySqlCommand("SELECT * FROM tweets", conn))
                {
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var tweet = new TweetInfo();

                        tweet.ID = reader.GetInt64(0);

                        tweet.Text = reader.GetString(6);

                        tweet.RetweetsScore = reader.GetDouble(15);
                        tweet.FavouritesScore = reader.GetDouble(16);
                        tweet.RelevantWordsRatio = reader.GetDouble(17);
                        tweet.SentimentScore = reader.GetDouble(18);

                        tweet.CredibilityScore = reader.GetFloat(8);

                        dataset.Add(tweet);
                    }

                    reader.Close();
                }

                conn.Close();
            }

            return dataset;
        }

        public static void ComputeCredibilityScoreWithNN(List<TweetInfo> tweets)
        {
            var nn = NeuralNetwork.LoadNetwork(File.ReadAllText(modelsLocation + "model_100k_1output.txt"));

            using (var conn = new MySqlConnection(CONNECTION_STRING))
            {
                conn.Open();

                foreach (var tweet in tweets)
                {
                    using (var cmd = new MySqlCommand("UPDATE tweets SET  credibility_score_nn = ?score WHERE id = ?id", conn))
                    {
                        var output = nn.Run(tweet.Input());

                        cmd.Parameters.Add("?id", MySqlDbType.Int64).Value = tweet.ID;
                        cmd.Parameters.Add("?score", MySqlDbType.Double).Value = output[0];

                        cmd.ExecuteNonQuery();
                    }
                }

                conn.Close();
            }
        }

        public static double GetUserCredibility(bool hasLocation, bool hasUrl, bool hasDescription, bool isVerified, bool geoEnabled, double createdAt, double last10Avg)
        {
            double score = 0.0d;

            if (hasLocation)
            {
                score += 0.01d;
            }

            if (hasUrl)
            {
                score += 0.01d;
            }

            if (hasDescription)
            {
                score += 0.03d;
            }

            if (isVerified)
            {
                score += 0.01d;
            }

            if (geoEnabled)
            {
                score += 0.08d;
            }

            score += createdAt * 0.07d;

            //score += followersScore * 0.1d;
            //score += favouritesScore * 0.1d;
            score += last10Avg * 0.7d;

            return score;
        }

        public static void ComputeUsersCredibilityScores()
        {
            // for all users
            // compute formula
            // update db

            using (var conn = new MySqlConnection(CONNECTION_STRING))
            {
                conn.Open();

                using (var cmd = new MySqlCommand("SELECT * FROM users", conn))
                {
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        long userId = reader.GetInt64(0);
                        bool hasLocation = reader.GetInt32(1) == 1;
                        bool hasUrl = reader.GetInt32(2) == 1;
                        bool hasDescription = reader.GetInt32(3) == 1;
                        bool isVerified = reader.GetInt32(4) == 1;
                        bool geoEnabled = reader.GetInt32(10) == 1;

                        double createdAt = reader.GetFloat(9) / ((DateTime.Now - DateTime.Parse("July 15, 2006")).TotalDays / 30);

                        double last10Avg = 0;

                        using (var conn2 = new MySqlConnection(CONNECTION_STRING))
                        {
                            conn2.Open();
                            using (var last = new MySqlCommand("SELECT avg(credibility_score_nn) FROM tweets WHERE author = ?authorId", conn2))
                            {
                                last.Parameters.Add("?authorId", MySqlDbType.Int64).Value = userId;
                                var reader2 = last.ExecuteReader();

                                reader2.Read();

                                last10Avg = reader2.GetDouble(0);

                                reader2.Close();
                            }

                            using (var u = new MySqlCommand("UPDATE users SET credibility_score = ?score WHERE id = ?id", conn2))
                            {
                                double score = GetUserCredibility(hasLocation, hasUrl,
                                    hasDescription, isVerified,
                                    geoEnabled, createdAt, last10Avg);

                                u.Parameters.Add("?id", MySqlDbType.Int64).Value = userId;
                                u.Parameters.Add("?score", MySqlDbType.Double).Value = score;

                                u.ExecuteNonQuery();
                            }
                            conn2.Close();
                        }
                    }

                    reader.Close();
                }

                conn.Close();
            }
        }

        // sentiments
        public static void Version5(int trainingDataCount)
        {
            Console.WriteLine("Version 5...");
            Training(trainingDataCount, "model_100k_vers5_" + trainingDataCount + ".txt", 5, new int[] { 5, 13, 1 });
        }

        // hashtags
        public static void Version4(int trainingDataCount)
        {
            Console.WriteLine("Version 4...");
            Training(trainingDataCount, "model_100k_vers4_" + trainingDataCount + ".txt", 4, new int[] { 5, 13, 1 });
        }

        // emoticons
        public static void Version3(int trainingDataCount)
        {
            Console.WriteLine("Version 3...");
            Training(trainingDataCount, "model_100k_vers3_" + trainingDataCount + ".txt", 3, new int[] { 4, 13, 1 });
        }

        public static void Version45(int trainingDataCount)
        {
            Console.WriteLine("Version 4 + 5...");
            Training(trainingDataCount, "model_100k_vers4_5_" + trainingDataCount + ".txt", 34, new int[] { 6, 13, 1 });
        }

        // no retweets
        public static void Version2(int trainingDataCount)
        {
            Console.WriteLine("Version 2...");
            Training(trainingDataCount, "model_100k_vers2_" + trainingDataCount + ".txt", 2, new int[] { 3, 13, 1 }, true);          
        }

        public static void Version1(int trainingDataCount)
        {
            Console.WriteLine("Version 1...");
            Training(trainingDataCount, "model_100k_vers1_" + trainingDataCount + ".txt", 1, new int[] { 3, 13, 1 });
        }

        private static List<TweetInfo> GetTrainingSet(List<TweetInfo> dataset, int trainingDataCount)
        {
            return dataset.Take(trainingDataCount).ToList();
        }
        
        private static List<TweetInfo> GetTestSet(List<TweetInfo> dataset, List<TweetInfo> trainingSet, int trainingDataCount)
        {
            return dataset.Where(t => !trainingSet.Contains(t)).Take(Math.Min(trainingDataCount / 3, dataset.Count - trainingDataCount)).ToList();
        }

        private static List<double> GetFirstLayerInput(TweetInfo tweet, int configuration)
        {
            List<double> ret;
            switch (configuration)
            {
                case 1:
                    ret = tweet.InputNoSentiments();
                    break;
                case 2:
                    ret = tweet.InputNoSentiments();
                    break;
                case 3:
                    ret = tweet.Input();
                    break;
                case 4:
                    ret = tweet.InputHashTags2();
                    break;
                case 5:
                    ret = tweet.InputHashTags1();
                    break;
                default: //case 45
                    ret = tweet.InputHashTags12();
                    break;
            }
            return ret;
        }

        private static void Training(int trainingDataCount, string modelFilePath, int configuration, int[] layersNodeCount, bool removeRetweets = false)
        {
            using (StreamWriter sw = File.AppendText(resultPath))
            {
                sw.WriteLine();
                sw.WriteLine(modelFilePath);
            }

            var dataset = GetDataset();
            if (removeRetweets)
            {
                dataset.RemoveAll((tweet) => tweet.Text.StartsWith("RT"));
            }
            var trainingSet = GetTrainingSet(dataset, trainingDataCount);
            var testSet = GetTestSet(dataset, trainingSet, trainingDataCount);

            var nn = new NeuralNetwork(0.01, layersNodeCount);

            for (int i = 0; i < 100000; ++i)
            {
                trainingSet.Shuffle();

                foreach (var tweet in trainingSet)
                {
                    nn.Train(GetFirstLayerInput(tweet, configuration), tweet.Output());
                }
            }

            using (var f = new FileStream(modelsLocation + modelFilePath, FileMode.Create))
            {
                nn.SaveModel(f);
            }

            float total = 0;

            foreach (var tweet in testSet)
            {
                var output = nn.Run(GetFirstLayerInput(tweet, configuration));

                bool match = (tweet.CredibilityScore == 1 && output[0] >= 0.6f) || (tweet.CredibilityScore == 0 && output[0] < 0.6f);

                if (match)
                {
                    total += 1;
                }
            }

            using (StreamWriter sw = File.AppendText(resultPath))
            {
                sw.WriteLine("Total 1: " + (total / testSet.Count()));
            }

            total = 0;
            foreach (var tweet in dataset)
            {
                var output = nn.Run(GetFirstLayerInput(tweet, configuration));
                bool match = (tweet.CredibilityScore == 1 && output[0] >= 0.6f) || (tweet.CredibilityScore == 0 && output[0] < 0.6f);

                if (match)
                {
                    total += 1;
                }
            }

            using (StreamWriter sw = File.AppendText(resultPath))
            {
                sw.WriteLine("Total 2: " + (total / dataset.Count()));
                sw.WriteLine("Testing done");
                sw.WriteLine();
            }
        }
        
        public static void GetCredibilityWithFormula(int trainingDataCount)
        {
            var dataset = GetDataset();
            dataset = dataset.Take(trainingDataCount).ToList();

            float total = 0;
            foreach (var tweet in dataset)
            {
                var output = 0.1f * tweet.RetweetsScore + 0.5f * tweet.FavouritesScore + 0.3f * tweet.RelevantWordsRatio + 0.1f * tweet.SentimentScore;

                bool match = (tweet.CredibilityScore == 1 && output >= 0.6f) || (tweet.CredibilityScore == 0 && output < 0.6f);

                if (match)
                {
                    total += 1;
                }
            }

            using (StreamWriter sw = File.AppendText(resultPath))
            {
                sw.WriteLine("Formula: " + (total / dataset.Count()));
                sw.WriteLine("Testing done");
                sw.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            resultPath = modelsLocation + "result.txt";
            if (!File.Exists(resultPath))
            {
                using (StreamWriter sw = File.CreateText(resultPath))
                {
                    sw.WriteLine(DateTime.Now);
                }
            }

            for (int trainingTweetsCount = 500; trainingTweetsCount <= 1500; trainingTweetsCount += 500)
            {
                Version1(trainingTweetsCount);
                Version2(trainingTweetsCount);
                Version3(trainingTweetsCount);
                Version4(trainingTweetsCount);
                Version45(trainingTweetsCount);
                Version5(trainingTweetsCount);
                GetCredibilityWithFormula(trainingTweetsCount);
            }

            using (StreamWriter sw = File.AppendText(resultPath))
            {
                sw.WriteLine(DateTime.Now);
                sw.WriteLine();
            }
            return;
        }

    }

    public static class Extension
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            int n = list.Count;
            while (n > 1)
            {
                UInt16 scale = UInt16.MaxValue;
                byte[] box = new byte[2];
                do
                {
                    provider.GetBytes(box);
                    scale = BitConverter.ToUInt16(box, 0);
                } while (!(scale < n * (UInt16.MaxValue / n)));
                int k = (scale % n);
                n--;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
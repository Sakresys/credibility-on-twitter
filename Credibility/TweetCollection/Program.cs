﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Tweetinvi.Models;
using TwitterCredibilityAPI_Server;
using edu.stanford.nlp.pipeline;
using static edu.stanford.nlp.ling.CoreAnnotations;
using edu.stanford.nlp.ling;
using System.Linq;
using java.util;
using System.Text;
using static TwitterCredibilityAPI_Server.TwitterAPI;

namespace TweetCollection
{
    class Program
    {
        private static readonly string ConnectionString = "server=localhost;userid=root;database=twitter_credibility";
        private static MySqlConnection mySqlConnection;
        private static string modelsLocation = @"..\..\..\Modele Antrenate\";

        static void Main(string[] args)
        {
            string consumerKey = "";
            string consumerSecret = "";
            string accessToken = "";
            string accessTokenSecret = "";
            var cred = new TwitterCredentials(consumerKey, consumerSecret, accessToken, accessTokenSecret);
            TwitterAPI.Initialize();
            CredibilityHelper.Initialize(File.ReadAllText(modelsLocation + "model_100k_vers5_1000.txt"));

            //Collect training data
            //CollectTrainingData(cred);

            //Update sentiment score
            //var dataset = UsersCredibility.Program.GetDataset();
            //UpdateSentimentScore(dataset);

            //Collect statistics data
            var stream = Tweetinvi.Stream.CreateSampleStream(cred);

            stream.TweetReceived += Stream_TweetReceived;
            stream.StreamStarted += Stream_StreamStarted;
            stream.StreamStopped += Stream_StreamStopped;
            stream.DisconnectMessageReceived += Stream_DisconnectMessageReceived;

            int retryCount = 0;
            while (true)
            {
                Console.WriteLine("Connecting to Twitter...");
                var t = stream.StartStreamAsync();

                t.Wait();

                int retryDelay = 1000 + retryCount * 1000;

                Console.WriteLine(string.Format("Trying to reconnect in {0} seconds (retry {1})", (int)Math.Ceiling((double)retryCount / 1000), retryCount));
                Thread.Sleep(retryDelay);

                if (retryCount > 15)
                {
                    Console.WriteLine("Reset retryCount");
                    retryCount = 0;
                }
            }
        }


        ///////////////////////////////////////////////////////////////////////////////////////TRAINING DATA/////////////////////////////////////////////
        private static string[] users = new string[]
        {
            "realDonaldTrump",
            "TheEllenShow",
            "Discovery",
            "NASA",
            "Eminem",
            "BarackObama",
            "NatGeo",
            "CNN",
            "MileyCyrus",
            "KimKardashian",
            "justinbieber",
            "BorisJohnson",
            "HillaryClinton",
            "tim_cook",
            "elonmusk",
            "jtimberlake",
            "DMX",
            "JKCorden",
            "jimmyfallon",
            "Google",
            "BillGates",
            "SnoopDogg",
            "Simona_Halep",
            "TEDTalks",
            "Android",
            "Microsoft",
            "KSIOlajidebt",
            "MariaSharapova",
            //new
            "nytimes",
            "instagram",
            "TheEconomist",
            "Reuters",
            "mashable",
            "big_picture",
            "wired",
            "foxnews",
            "mtv",
            "lifehacker",
            "wsj",
            "time",
            "katyperry",
            "KevinHart4real",
            "ConanOBrien",
            "DalaiLama",
            "LeoDiCaprio",
            "timberners_lee",
            "noamchomskyT",
            "KingJames",
            "ladygaga",
            "rihanna"
        };

        private static void CollectTrainingData(TwitterCredentials cred)
        {
            try
            {
                if (mySqlConnection == null)
                {
                    mySqlConnection = new MySqlConnection(ConnectionString);
                    mySqlConnection.Open();
                    Console.WriteLine("Datababase connection opened!");
                }

                int tweetsCount = 40;
                for (int i = 0; i < users.Length; ++i)
                {
                    var user = TwitterAPI.GetUserInfo(users[i], tweetsCount);

                    using (var q = new MySqlCommand("INSERT INTO users (id, has_location, has_url, has_description, is_verified, screen_name, followers, last_tweets, created_at, geo_enabled) VALUES (?id, ?has_location, ?has_url, ?has_description, ?is_verified, ?screen_name, ?followers, ?last_tweets, ?created_at, ?geo_enabled)", mySqlConnection))
                    {
                        q.Parameters.Add("?id", MySqlDbType.Int64).Value = user.ID;
                        q.Parameters.Add("?has_location", MySqlDbType.Int32).Value = user.HasLocation == true ? 1 : 0;
                        q.Parameters.Add("?has_url", MySqlDbType.Int32).Value = user.HasUrl == true ? 1 : 0;
                        q.Parameters.Add("?has_description", MySqlDbType.Int32).Value = user.HasDescription == true ? 1 : 0;
                        q.Parameters.Add("?is_verified", MySqlDbType.Int32).Value = user.IsVerified == true ? 1 : 0;
                        q.Parameters.Add("?screen_name", MySqlDbType.Text).Value = user.ScreenName;
                        q.Parameters.Add("?followers", MySqlDbType.Int32).Value = user.FollowersNumber;
                        StringBuilder tweets = new StringBuilder();
                        for (int j = 0; j < user.LastTweets.Count; ++j)
                        {
                            tweets.Append(user.LastTweets[j].ID);
                            if (j != user.LastTweets.Count - 1)
                            {
                                tweets.Append(",");
                            }
                        }
                        q.Parameters.Add("?last_tweets", MySqlDbType.Text).Value = tweets;
                        q.Parameters.Add("?created_at", MySqlDbType.Double).Value = user.CreatedAt;
                        q.Parameters.Add("?geo_enabled", MySqlDbType.Int32).Value = user.GeoEnabled == true ? 1 : 0;

                        q.ExecuteNonQuery();
                    }

                    for (int j = 0; j < (user.LastTweets.Count > tweetsCount ? tweetsCount : user.LastTweets.Count); ++j)//new: switched from 10 to 40
                    {
                        var tweet = user.LastTweets[j];

                        using (var q = new MySqlCommand("INSERT INTO tweets (id, author, retweets, favorites, content, created_at, words_count, relevant_words_count, characters_count, rt_score, fav_score, rwr_score) VALUES (?id, ?author, ?retweets, ?favorites, ?content, ?created_at, ?words_count, ?relevant_words_count, ?characters_count, ?rt_score, ?fav_score, ?rwr_score)", mySqlConnection))
                        {
                            q.Parameters.Add("?id", MySqlDbType.Int64).Value = tweet.ID;
                            q.Parameters.Add("?author", MySqlDbType.Int64).Value = tweet.AuthorID;
                            q.Parameters.Add("?retweets", MySqlDbType.Int32).Value = tweet.RetweetsNumber;
                            q.Parameters.Add("?favorites", MySqlDbType.Int32).Value = tweet.FavoritesNumber;
                            q.Parameters.Add("?content", MySqlDbType.Text).Value = tweet.Text;
                            q.Parameters.Add("?created_at", MySqlDbType.Timestamp).Value = tweet.CreatedAt;
                            q.Parameters.Add("?words_count", MySqlDbType.Int32).Value = tweet.WordsCount;
                            q.Parameters.Add("?relevant_words_count", MySqlDbType.Int32).Value = tweet.RelevantWordsCount;
                            q.Parameters.Add("?characters_count", MySqlDbType.Int32).Value = tweet.CharactersCount;
                            q.Parameters.Add("?rt_score", MySqlDbType.Double).Value = tweet.RetweetsScore;
                            q.Parameters.Add("?fav_score", MySqlDbType.Double).Value = tweet.FavouritesScore;
                            q.Parameters.Add("?rwr_score", MySqlDbType.Double).Value = tweet.RelevantWordsRatio;

                            q.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error");
            }
            finally
            {
                if (mySqlConnection != null)
                {
                    mySqlConnection.Close();
                }
            }
        }

        public static void UpdateSentimentScore(List<TweetInfo> tweets)
        {
            try
            {
                if (mySqlConnection == null)
                {
                    mySqlConnection = new MySqlConnection(ConnectionString);
                    mySqlConnection.Open();
                    Console.WriteLine("Datababase connection opened!");
                }

                foreach (var tweet in tweets)
                {
                    using (var cmd = new MySqlCommand("UPDATE tweets SET senti_score = ?score WHERE id = ?id", mySqlConnection))
                    {
                        cmd.Parameters.Add("?id", MySqlDbType.Int64).Value = tweet.ID;
                        cmd.Parameters.Add("?score", MySqlDbType.Double).Value = GetTweetSentiment(tweet.Text);

                        cmd.ExecuteNonQuery();
                    }
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine("Error");
            }
            finally
            {
                if (mySqlConnection != null)
                {
                    mySqlConnection.Close();
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////STATISTICS DATA/////////////////////////////////////////////

        private static void Stream_DisconnectMessageReceived(object sender, Tweetinvi.Events.DisconnectedEventArgs e)
        {
            Console.WriteLine("Twitter disconnected!");
        }

        private static void Stream_StreamStopped(object sender, Tweetinvi.Events.StreamExceptionEventArgs e)
        {
            if (mySqlConnection != null)
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
                mySqlConnection = null;
                Console.WriteLine("Datababase connection closed!");
            }

            Console.WriteLine("Stream stopped");
        }

        private static void Stream_StreamStarted(object sender, EventArgs e)
        {
            try
            {
                if (mySqlConnection == null)
                {
                    mySqlConnection = new MySqlConnection(ConnectionString);
                    mySqlConnection.Open();
                    Console.WriteLine("Datababase connection opened!");
                }
            }
            catch
            {

            }

            Console.WriteLine("Connected to Twitter!");
        }

        private static void Stream_TweetDeleted(object sender, Tweetinvi.Events.TweetDeletedEventArgs e)
        {
        }

        private static void Stream_TweetReceived(object sender, Tweetinvi.Events.TweetReceivedEventArgs e)
        {
            if (e.Tweet.Coordinates != null && e.Tweet.Language == Language.English)
            {
                TwitterAPI.TweetInfo tweet = TwitterAPI.ConvertTweet(e.Tweet);
                double tweetScore = CredibilityHelper.GetCredibilityScore(tweet);
                double tweetLongitude = e.Tweet.Coordinates.Longitude;
                double tweetLatitude = e.Tweet.Coordinates.Latitude;

                try
                {
                    if (mySqlConnection == null)
                    {
                        mySqlConnection = new MySqlConnection(ConnectionString);
                        mySqlConnection.Open();
                        Console.WriteLine("Datababase connection opened!");
                    }

                    using (var q = new MySqlCommand("INSERT INTO tweet_collection (tweet_id, user_id, text, credibility_score, retweets_score, favourites_score, relevant_words_ratio, sentiment_score, longitude, latitude) VALUES (?tweet_id, ?user_id, ?text, ?credibility_score, ?retweets_score, ?favourites_score, ?relevant_words_ratio, ?sentiment_score, ?longitude, ?latitude)", mySqlConnection))
                    {
                        q.Parameters.Add("?tweet_id", MySqlDbType.Int64).Value = tweet.ID;
                        q.Parameters.Add("?user_id", MySqlDbType.Int64).Value = e.Tweet.CreatedBy.Id;
                        q.Parameters.Add("?text", MySqlDbType.Text).Value = tweet.Text;
                        q.Parameters.Add("?credibility_score", MySqlDbType.Double).Value = tweetScore;
                        q.Parameters.Add("?retweets_score", MySqlDbType.Double).Value = tweet.RetweetsScore;
                        q.Parameters.Add("?favourites_score", MySqlDbType.Double).Value = tweet.FavouritesScore;
                        q.Parameters.Add("?relevant_words_ratio", MySqlDbType.Double).Value = tweet.RelevantWordsRatio;
                        q.Parameters.Add("?sentiment_score", MySqlDbType.Double).Value = tweet.SentimentScore;
                        q.Parameters.Add("?longitude", MySqlDbType.Double).Value = tweetLongitude;
                        q.Parameters.Add("?latitude", MySqlDbType.Double).Value = tweetLatitude;

                        q.ExecuteNonQuery();

                        Console.WriteLine("Tweet inserted!");
                    }
                }
                catch
                {
                    Console.WriteLine("Error at receiving tweet!");
                }
            }
        }
    }
}

# README #

The scope of this project is to determine if a tweet is credible or not and conclude the credibility of the author.
For that tweets were collected using Twitter's API.
The project contains multiple modules: collecting data, training data, create statistics, API and web app to use the API.

### NLP models ###
In order to use TwitterCredibility(API) and Training modules you will need to include the Stanford Core NLP models.
The project uses version 3.8.0. The modules must have the same version as the Nuget package installed.
Download link for the modules: nlp.stanford.edu/software/stanford-english-corenlp-2017-06-09-models.jar.
The .jar file must be extracted and the resulting folder have to placed at root solution level.

### Data ###
Inside "annotated data" folder is placed the data used to train the neural network model. It contains the tweet id, author, text and annotated label (0 - not credible, 1 - credible).
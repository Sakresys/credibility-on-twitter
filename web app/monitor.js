var credibilityData;

function pageLoad(){	
	
	var searchRegex = /\?id=[0-9]+/g;

	if(window.location.search.search(searchRegex) == 0)
	{
		var index = window.location.search.indexOf("=");
		var id = window.location.search.substring(index+1);
		if(!isNaN(id))
		{
			httpGetAsync('http://127.0.0.1:1231/credibility/monitor/' + id, constructChartAndTable);
		}
		else
		{
			constructEmptyChart();
		}
	}
	else
	{
		constructEmptyChart();
	}
}

function httpGetAsync(theUrl, callback)
{
	var xhr = new XMLHttpRequest();
	
	xhr.onload  = function (e) {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
			  callback(xhr.responseText);
			} else {
			  constructEmptyChart();
			}
		}
	};
	xhr.onerror = function (e) {
		constructEmptyChart();
	};
	xhr.open("GET", theUrl, true);
	xhr.send(null);
}

function constructEmptyChart()
{
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(function (){
		drawChart();
	});
}

function constructChartAndTable(data)
{
	credibilityData = JSON.parse(data);
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(function (){
		drawChart(data);
	});
	constructTable(data);
}

function drawChart(data) {

	var chartData = new google.visualization.DataTable();
	chartData.addColumn('datetime', 'X');
	chartData.addColumn('number', 'Credibility');

	if(data === undefined)
	{
	}
	else
	{
		if(credibilityData.Type == "User")
		{
			document.getElementById("type").innerHTML = "Credibility of the user";
			document.getElementById("identifier").innerHTML = "@" + credibilityData.Subject;

			if(credibilityData.Score != -1)
			{		
				var i;	
				var rowCount = (credibilityData.History.length > 24) ? 24 : credibilityData.History.length;
				for(i = 0; i < rowCount; i++)
				{	
					var hasTweets = credibilityData.History[i].hasOwnProperty('Tweets');
					if(hasTweets)
					{
						hasTweets = hasTweets && (credibilityData.History[i].Tweets.length > 0);
						if(hasTweets)
						{
							if(i == 0)
							{
								document.getElementById("date1").innerHTML = " between " + credibilityData.StartDateTime;
								document.getElementById("date2").innerHTML = " and " + credibilityData.History[i].Tweets[0].CheckDateTime;
							}

							chartData.addRow([new Date(credibilityData.History[i].Tweets[0].CheckDateTime), credibilityData.History[i].Score]);		
						}					
					}	
				}								
			}
		}
		else if(credibilityData.Type == "Tweet")
		{
			document.getElementById("type").innerHTML = "Credibility of the tweet";
			document.getElementById("identifier").innerHTML = credibilityData.Subject;
			document.getElementById("identifier").addEventListener("mouseover", tweetMouseOver);
			document.getElementById("identifier").addEventListener("mouseout", tweetMouseOut);

			if(credibilityData.Score != -1)
			{						
				document.getElementById("date1").innerHTML = " between " + credibilityData.StartDateTime;
				document.getElementById("date2").innerHTML = " and " + credibilityData.History[0].CheckDateTime;
				
				var i;
				var rowCount = (credibilityData.History.length > 24) ? 24 : credibilityData.History.length;
				for(i = 0; i < rowCount; i++)
				{
					chartData.addRow([new Date(credibilityData.History[i].CheckDateTime), credibilityData.History[i].Score]);	
				}								
			}
		}
	}
	
	var options = {
	  title: 'Evolution of credibility',
	  hAxis: {
	    title: 'Timestamp'
	  },
	  vAxis: {
	    title: 'Credibility'
	  }
	};

	var chart = new google.visualization.LineChart(document.getElementById('chart'));

	chart.draw(chartData, options);
}

function constructTable(data)
{
	if(data === undefined)
	{
	}
	else
	{
		if(credibilityData.Type == "User")
		{
			if(credibilityData.Score != -1)
			{	
				var table = document.createElement('table');
				table.id = 'cotentTable';
				createHeader(table, false);

				var i;	
				var rowCount = (credibilityData.History.length > 24) ? 24 : credibilityData.History.length;
				for(i = 0; i < rowCount; i++)
				{	
					var hasTweets = credibilityData.History[i].hasOwnProperty('Tweets');
					if(hasTweets)
					{
						hasTweets = hasTweets && (credibilityData.History[i].Tweets.length > 0);
						if(hasTweets)
						{
							var row = createDOMElement(table, 'tr');
							row.id = "row" + i;

							var node = createDOMElement(row, 'td', 'tweets');					
							node = createDOMElement(node, 'i', 'down');	
							node.id = i;				
							node.addEventListener("click", addClickEvent, false);

							insertRows(row, i, false, rowCount);
						}					
					}	
				}

				var container = document.getElementById("content"); 
  				container.appendChild(table);							
			}
		}
		else if(credibilityData.Type == "Tweet")
		{
			if(credibilityData.Score != -1)
			{	
				var table = document.createElement('table');
				createHeader(table, true);

				var i;
				var rowCount = (credibilityData.History.length > 24) ? 24 : credibilityData.History.length;
				for(i = 0; i < rowCount; i++)
				{
					var row = createDOMElement(table, 'tr');
					insertRows(row, i, true, rowCount);				
				}

				var container = document.getElementById("content"); 
  				container.appendChild(table); 								
			}
		}
	}
}

function tweetMouseOver(coordinates)
{
	var tweet = document.getElementById("tweet");
	if(tweet.style.display != "block")
	{
		tweet.style.display = "block";
		tweet.style.opacity = "0";
		//added 20 because twitter pops up over pointer and triggers mouseout event
		var x = coordinates.pageX + 20;
	    var y = coordinates.pageY + 20;    
	    tweet.style.left = x + "px";
	    tweet.style.top = y + "px";

	    var id = this.innerHTML;
	    twttr.widgets.createTweet(
	      id, tweet, 
	      {
	        conversation : 'none',
	        cards        : 'hidden',
	        linkColor    : '#cc0000',
	        theme        : 'light'
	      })
	    .then( function( el ) {
	    	if(y + tweet.clientHeight > window.innerHeight + window.scrollY)
	    	{
	    		tweet.style.top = y - 40 - tweet.clientHeight + "px";
	    	}
	    	tweet.style.opacity = "1";
		});
	}
}

function tweetMouseOut()
{
	var tweet = document.getElementById("tweet");
	tweet.style.display = "none";
	while (tweet.firstChild) {
    	tweet.removeChild(tweet.firstChild);
	}
}

function createHeader(table, isTweet)
{
	var row = createDOMElement(table, 'tr');
	var node;
	if(!isTweet)
	{
		node = createDOMElement(row, 'th');
		createTextElement(node, "Tweets(ID)");
	}
	node = createDOMElement(row, 'th');
	createTextElement(node, "Timestamp");
	node = createDOMElement(row, 'th');
	createTextElement(node, "Credibility");
	node = createDOMElement(row, 'th');
	createTextElement(node, "Difference last hour");
}

function insertRows(row, i, isTweet, rowCount)
{
	var node = createDOMElement(row, 'td');
	if(isTweet)
	{
		createTextElement(node, credibilityData.History[i].CheckDateTime);
	}
	else
	{
		createTextElement(node, credibilityData.History[i].Tweets[0].CheckDateTime);
	}

	node = createDOMElement(row, 'td');
	createTextElement(node, credibilityData.History[i].Score);

	if(i != rowCount - 1)
	{
		var difference = credibilityData.History[i].Score - credibilityData.History[i+1].Score;
		difference = difference.toFixed(6);
		var higher = (difference >= 0) ? true : false;
		node = createDOMElement(row, 'td', higher ? "higher" : "lower");					
		createTextElement(node, difference);
	}	
	else
	{
		node = createDOMElement(row, 'td', "higher");
		createTextElement(node, Number(0).toFixed(6));
	}
}

function createDOMElement(parent, element, nameOfClass)
{
	var node = document.createElement(element);
	if(nameOfClass != undefined)
	{
		node.className = nameOfClass;
	}
	parent.appendChild(node);
	return node;
}

function createTextElement(node, text)
{
	var textNode = document.createTextNode(text);
	node.appendChild(textNode);
}

function addClickEvent()
{
	var table = document.getElementById('cotentTable');
	var shouldExpand = (this.className == "down");
	var index = Number(this.id);

	if(shouldExpand)
	{
		this.className = "up";
		var hasTweets = credibilityData.History[index].hasOwnProperty('Tweets');
		if(index + 2 < table.childElementCount)
		{					
			if(hasTweets)
			{
				var i;
				for(i = 0; i < credibilityData.History[index].Tweets.length; i++)
				{	
					var rowAfter = document.getElementById("row"+(index + 1));
					var row = document.createElement('tr');
					table.insertBefore(row, rowAfter);
					insertSubrows(row, i, index);							
				}	
			}
		}
		else//last row in the table
		{
			if(hasTweets)
			{
				var i;
				for(i = 0; i < credibilityData.History[index].Tweets.length; i++)
				{	
					var row = createDOMElement(table, 'tr');
					insertSubrows(row, i, index);						
				}
			}
		}
	}
	else
	{
		this.className = "down";

		var rowsToRemove = credibilityData.History[index].Tweets.length;
		var row = document.getElementById("row"+index);
		var i;
		for(i = 0; i < rowsToRemove; i++)
		{
			table.deleteRow(row.rowIndex + 1);
		}
	}
}


function insertSubrows(row, i, index)
{
	var node = createDOMElement(row, 'td', 'tweets');
	createTextElement(node, credibilityData.History[index].Tweets[i].Id);
	node.addEventListener("mouseover", tweetMouseOver);
	node.addEventListener("mouseout", tweetMouseOut);

	node = createDOMElement(row, 'td');
	createTextElement(node, credibilityData.History[index].Tweets[i].CheckDateTime);

	node = createDOMElement(row, 'td');
	createTextElement(node, credibilityData.History[index].Tweets[i].Score);

	node = createDOMElement(row, 'td');
	createTextElement(node, "");
}

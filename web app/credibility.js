function validateInput()
{
	document.getElementById('errorMessage').style.display = "none";
	document.getElementById('score').style.display = "none";
	document.getElementById('link').style.display = "none";

	document.getElementById('errorMessageText').innerHTML = ""; 
	document.getElementById('scoreText').innerHTML = ""; 
	document.getElementById('linkText').href = "#"; 

	if(document.getElementById('identifier').value == "")
	{
		displayError("no ID entered");		
	}
	else if(document.getElementById("user").checked == false && document.getElementById("tweet").checked == false)
	{
		displayError("no checkbox checked"); 
	}
	else
	{
		var texts = document.getElementById('identifier').value.split(" ");
		if(texts.length > 1)
		{
			displayError("no whitespaces"); 
		}
		else
		{
			return true;
		}
	}
	return false;
}

function monitorClick(){
	var shouldContinue = validateInput();
	if(shouldContinue)
	{
		if(document.getElementById("user").checked == true)
		{
			httpPOSTAsync("http://127.0.0.1:1231/credibility/users/monitor/" + document.getElementById('identifier').value);
		}
		else if(document.getElementById("tweet").checked == true)
		{
			httpPOSTAsync("http://127.0.0.1:1231/credibility/tweets/monitor/" + document.getElementById('identifier').value);
		}
	}
}

function httpPOSTAsync(theUrl)
{
	var xhr = new XMLHttpRequest();
	
	xhr.onload  = function (e) {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				var data = JSON.parse(xhr.responseText);
				if(data.hasOwnProperty('monitoringRequestId'))
				{
					document.getElementById('link').style.display = "block";
					document.getElementById('linkText').href = "./monitor.html?id=" + data.monitoringRequestId;
				}
				else
				{
					displayError("something went wrong");
				}
			} else if(xhr.status === 404){
				displayError("ID not found");
			} else {
				displayError("something went wrong");
			}
		}
	};
	xhr.onerror = function (e) {
		displayError("something went wrong");
	};

	xhr.open("POST", theUrl, true);	
	xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
	//xhr.setRequestHeader('Content-type','application/json');
	//xhr.setRequestHeader('Accept','application/json');
	xhr.send(null);
}

function getScoreClick(){
	var shouldContinue = validateInput();
	if(shouldContinue)
	{
		if(document.getElementById("user").checked == true)
		{
			httpGetAsync("http://127.0.0.1:1231/credibility/users/" + document.getElementById('identifier').value);
		}
		else if(document.getElementById("tweet").checked == true)
		{
			httpGetAsync("http://127.0.0.1:1231/credibility/tweets/" + document.getElementById('identifier').value);
		}
	}
}

function httpGetAsync(theUrl)
{
	var xhr = new XMLHttpRequest();
	
	xhr.onload  = function (e) {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				var data = JSON.parse(xhr.responseText);
				document.getElementById('score').style.display = "block";
				document.getElementById('scoreText').innerHTML = data.credibility;
			} else if(xhr.status === 404){
				displayError("ID not found");
			} else {
				displayError("something went wrong");
			}
		}
	};
	xhr.onerror = function (e) {
		displayError("something went wrong");
	};

	xhr.open("GET", theUrl, true);
	xhr.send(null);
}

function displayError(message)
{
	document.getElementById('errorMessage').style.display = "block";
	document.getElementById('errorMessageText').innerHTML = "*" + message; 
}

